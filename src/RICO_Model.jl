module RICO_Model

using Distributions, Transducers
import Base.Threads.@threads
import Base.values

export Vox, grow, poke, random_return, daily_price_path, hourly_price_path, vox_path

grow(amt, ray, dt) = amt * ray^dt

struct Vox
    timestamp::Int
    par::Real
    mar::Real
    way::Real
    how::Real
    cap::Real
end
values(v::Vox) = (v.timestamp, v.par, v.mar, v.way, v.how, v.cap)
truncvalues(v::Vox) = (v.timestamp, v.par, v.mar, v.way)

function poke(v::Vox, p::Float64, t1::Integer)
    mar = p
    t0 = v.timestamp
    dt = t1 - t0
    @assert dt ≥ 0
    par = v.par * v.way^dt
    way = if mar < par
	min(v.cap, v.way * v.how ^ dt)
    elseif mar > par
	max(1 / v.cap, v.way *(1 / v.how)^dt)
    else
	v.way
    end
    Vox(
	t1,
	par,
	mar,
	way,
	v.how,
	v.cap
    )
end

function random_return(μ::Real, σ::Real, n::Integer)
    d = Normal(μ, σ)
    rand(d, n)
end

function daily_price_path(price_init::Real, μ::Real, σ::Real; n::Integer=500, annualized=true)
    μ = annualized ? μ / 365 : μ
    σ = annualized ? σ / sqrt(365) : σ
    rets = random_return(μ, σ, n)
    out = collect(Scan((p0,r1) -> p0*(1+r1), price_init), rets)
    vcat(price_init, out)
end

function hourly_price_path(price_init::Real, μ::Real, σ::Real; n::Integer=2400, annualized=true)
    μ = annualized ? μ / 365 / 24 : μ
    σ = annualized ? σ / sqrt(365 * 24) : σ
    rets = random_return(μ, σ, n)
    out = collect(Scan((p0,r1) -> p0*(1+r1), price_init), rets)
    vcat(price_init, out)
end

function vox_path(dpath, how, cap, arr=Vector{Union{Nothing, Vox}}(nothing, length(dpath)); freq = :hour)
    v = Vox(
        0,
        dpath[1],
        dpath[1],
        1.0,
        (1 + 1e-7 * how),
        (1 + 1e-8 * cap)
    )
    mult = freq == :hour ? 3600*24 : 3600
    for (i,p) in enumerate(dpath)
        v = poke(v, p, i*mult)
        arr[i] = v
    end
    arr
end
# Write your package code here.

end
