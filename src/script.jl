using RICO_Model
using Plots


function get_grid(h, c)
    out = []
    for x in h
        for y in c
            push!(out, (x,y))
        end
    end
    out
end

function vox_grid(dpath, h = .1:.1:2, c=.1:.1:2)
    x = length(h)
    y = length(c)
    vec() = Vector{Union{Nothing, Vox}}(nothing, length(dpath))
    out = Array{Union{Nothing,Vector{Union{Nothing, Vox}}}, 2}(nothing, x, y)
    for i=1:x
        for j=1:y
            out[i,j] = vec()
        end
    end
    for (i,h_) in enumerate(h)
        for (j, c_) in enumerate(c)
            vox_path(dpath, h_, c_, out[i,j])
        end
    end
    out
end

function gatherY(arr::Array, prop::Symbol)
    x,y = size(arr)
    out = Vector{Vector{Float64}}()
    for i=1:x
        for j=1:y
            yarr = [getproperty(z, prop) for z in arr[i,j]]
            push!(out, yarr)
        end
    end
    out
end
gatherY(vec::Vector, prop::Symbol) = [getproperty(z, prop) for z in vec]

ymar(arr) = gatherY(arr, :mar)
ypar(arr) = gatherY(arr, :par)
yway(arr) = gatherY(arr, :way)

function qplot(y)
    plot(ymar(y), label = "mar")
    plot!(ypar(y), label = "par")
end
function qsim(how, cap, S0=1500, μ=.0, σ=.5)
    hpath = hourly_price_path(S0, μ, σ)
    vox_path(hpath, how, cap)
end

# to run a quick sim:
y = qsim(1.1517, 2.2)
qplot(y)
